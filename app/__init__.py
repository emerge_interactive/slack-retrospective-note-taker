from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import logging

db = SQLAlchemy()
# migrate = Migrate()


def create_app():
    """Create Flask application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')

    db.init_app(app)
    # migrate.init_app(app, db)

    logging.basicConfig(level=app.config['LOGGING_LEVEL'], format=app.config['LOGGING_FORMAT'])

    with app.app_context():
        # Import parts of our application
        from .admin.views import admin
        from .slack_retro_note_api.views import slack_retro_note_api

        # Register Blueprints
        app.register_blueprint(admin)
        app.register_blueprint(slack_retro_note_api)

        # Initialize Global db
        db.create_all()

        return app
