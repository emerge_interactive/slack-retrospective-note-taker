from flask import current_app as app
import requests
import datetime


class ConfluenceAPI(object):
    def __init__(self):
        self._base_url = app.config['CONFLUENCE_BASE_URL']
        # build auth tuple for requests
        self._auth = (app.config['ATLASSIAN_AUTH_USER'], app.config['ATLASSIAN_AUTH_PASSWORD'])

    def _add_retro_note(self, original_content, retro_note, user_name):
        new_content = None

        app.logger.info('original_content: {}'.format(original_content))
        app.logger.info('retro note to be added: {}'.format(retro_note))

        created = datetime.datetime.now().strftime("%m/%d/%Y")
        # create an html table row out of the retro note
        note_html = '<tr><td>{}</td><td>{}</td><td>{}</td></tr>'.format(retro_note, created, user_name)

        # find the position of the end of the table body
        idx = original_content.index('</tbody>')
        app.logger.info('idx: {}'.format(idx))

        # create new string that is our original content with new row inserted at end of table
        new_content = original_content[:idx] + note_html + original_content[idx:]

        app.logger.info('updated content: {}'.format(new_content))

        return new_content

    def get_page_by_title(self, space_key, page_title):
        app.logger.info('get_page for space key: {}, title: {}'.format(space_key, page_title))

        params = {
            'spaceKey': space_key,
            'title': page_title,
            'expand': 'body.storage,version'
        }

        response = requests.get(self._base_url + 'content', params=params, auth=self._auth)

        app.logger.info('response status_code: ' + str(response.status_code))
        if not response or response.status_code != 200:
            app.logger.warn('Could not get confluence page title: {}, space key: {}'.format(page_title, space_key))
            app.logger.warn('Response text: {}'.format(response.text))
            return {'error': 'Could not get confluence page title: {}, space key: {}'.format(page_title, space_key)}

        app.logger.info('response json: ' + str(response.json()))
        return response.json()

    def get_page(self, page_id):
        app.logger.info('get_page by id: {}'.format(page_id))

        params = {
            'expand': 'body.storage,version'
        }

        url = '{}content/{}'.format(self._base_url, page_id)
        response = requests.get(url, params=params, auth=self._auth)

        app.logger.info('response status_code: ' + str(response.status_code))

        if not response or response.status_code != 200:
            app.logger.warn('Could not get confluence page id: {}'.format(page_id))
            app.logger.warn('Response text: {}'.format(response.text))
            return {'error': 'Could not get confluence page id: {}'.format(page_id)}

        app.logger.info('response json: ' + str(response.json()))

        return response.json()

    def update_retrospective_page_content(self, page_id, retro_note, user_name):
        app.logger.info('updating retro page content for page id: {}'.format(page_id))

        # get current page
        page_info = self.get_page(page_id)

        if 'error' in page_info:
            app.logger.warn('could not get retro page to update from confluence')
            app.logger.warn('page_info: {}'.format(page_info))
            return {'error': page_info['error']}

        # iterate version number
        next_version = page_info['version']['number'] + 1

        current_page_content = page_info['body']['storage']['value']

        # using a helper method to add the retro note to current page content
        page_content = self._add_retro_note(current_page_content, retro_note, user_name)

        # body of PUT request
        data = {
            'id': str(page_id),
            'type': 'page',
            'title': page_info['title'],
            'version': {
                'number': next_version
            },
            'body': {
                'storage': {
                    'representation': 'storage',
                    'value': page_content,
                }
            }
        }

        app.logger.info('data for PUT: {}'.format(data))

        headers = {
            'Content-type': 'application/json'
        }

        url = '{}content/{}'.format(self._base_url, page_id)

        # make API call
        response = requests.put(url, json=data, headers=headers, auth=self._auth)

        app.logger.info('update response status code: ' + str(response.status_code))

        if response.status_code != 200:
            app.logger.warn('failed request to update confluence page')
            app.logger.warn('response text', response.text)
            return {'error': str(response.status_code)}

        app.logger.info('update response json: ' + str(response.json()))

        # see if we can pull out the url of confluence page from response
        page_url = '{}{}'.format(page_info['_links']['base'], page_info['_links']['webui'])

        return {'success': page_url}
