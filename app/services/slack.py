from flask import request
from flask import current_app as app
import hmac
import hashlib


def verify_signature(timestamp, signature):
    """Verifies a slack signed secret - https://api.slack.com/docs/verifying-requests-from-slack
    """
    signing_secret = app.config['SLACK_SIGNING_SECRET']
    vnum = app.config['SLACK_VNUM']

    req = str.encode(vnum + ':' + str(timestamp) + ':') + request.get_data()

    app.logger.debug('concatenated slack req: {}'.format(req))

    request_hash = vnum + '=' + hmac.new(
        str.encode(signing_secret),
        req, hashlib.sha256
    ).hexdigest()

    app.logger.debug('computed slack request hash: {}'.format(request_hash))

    return hmac.compare_digest(request_hash, signature)


def format_slack_response_message(title, message_text, response_type='in_channel', color=None, title_link=None):
    """Helper for creating response in format slack wants
    """
    response = {
        'response_type': response_type,
        'attachments': [{
            'title': title,
            'text': message_text
        }]
    }

    # append optionals
    if color:
        response['attachments'][0]['color'] = color
    if title_link:
        response['attachments'][0]['title_link'] = title_link

    return response
