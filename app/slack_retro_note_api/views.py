from flask import Blueprint, json, request, jsonify
from flask import current_app as app
import os

from ..models.models import db, Mapping
from ..services.slack import verify_signature, format_slack_response_message
from ..services.confluence import ConfluenceAPI

slack_retro_note_api = Blueprint('slack_retro_note_api', __name__)


@slack_retro_note_api.route('/retro_note', methods=['POST'])
def create_retro_note():
    """Creates a retrospective note from a POST request, sent via slack
        'slash command'.
    """
    # init some return vars
    response_text = None
    error_text = None
    page_id = None

    skip_slack_validation = app.config['SKIP_SLACK_SIGNING']

    # get slack signing headers
    slack_signature = request.headers['X-Slack-Signature'] if 'X-Slack-Signature' in request.headers else None
    timestamp = request.headers['X-Slack-Request-Timestamp'] if 'X-Slack-Request-Timestamp' in request.headers else None

    if skip_slack_validation != 'True':
        # make sure signing headers are present
        if not slack_signature or not timestamp:
            valid_slack_request = False
            app.logger.error('Missing slack signing headers on request')
        else:
            # validate signing
            valid_slack_request = verify_signature(signature=slack_signature, timestamp=timestamp)
            app.logger.error('Slack request signature was invalid')

        app.logger.info('Slack request validation result: {}'.format(valid_slack_request))
    elif skip_slack_validation:
        valid_slack_request = True
        app.logger.info('Skipping slack request validation. SKIP_SLACK_SIGNING: {}'.format(skip_slack_validation))

    if not valid_slack_request:
        error_text = 'Your retrospective note could not be saved at this time (error: could not validate slack request).  Please manually add your note to the correct page in confluence.'
        app.logger.error(error_text)
        slack_message = format_slack_response_message('Error', message_text=error_text, color='danger')
        return jsonify(slack_message)

    # the next two must happen in sequential order
    request_body = request.get_data()
    form_data = request.form

    app.logger.info('raw post request body: {}'.format(request_body))

    # parse out relevant fields from slack POST
    note_text = form_data.get('text')
    channel_name = form_data.get('channel_name')
    # channel_id = form_data.get('channel_id')
    user_name = form_data.get('user_name')
    # response_url = form.data.get('response_url')

    # validate POST body for required params
    if not note_text or not channel_name or not user_name:
        error_text = 'Your retrospective note could not be saved at this time (error: missing params).  Please manually add your note to the correct page in confluence.'
        app.logger.error(error_text)
        slack_message = format_slack_response_message('Error', message_text=error_text, color='danger')
        return jsonify(slack_message)

    # TODO: validate format of retro note (<start|stop|continue> - <note text>)

    # if user has submitted a 'help' request, respond to that
    if note_text == 'help':
        app.logger.info('Responding to help request')
        return app.response_class(
            response=json.dumps({
                'response_type': 'ephermal',
                'text': 'How to use /retro',
                'attachments': [{
                    'text': "To add a retrospective note to the confluence page for this project channel, type '/retro' and then your retrospective note (example: '/retro Start: keeping a running log of retrospective notes').  This will save your note on a confluence page in the project space.  If it fails, make sure an administrator has added your project to this application."
                }]
            }),
            status=200,
            mimetype='application/json'
        )

    # find the confluence page id associated with this channel
    mapping = Mapping.query.filter_by(channel=channel_name).first()
    app.logger.debug('mapping returned for channel {}: {}'.format(channel_name, mapping))

    # ensure we have confluence page to record notes for this channel (lookup by channel name)
    if mapping and mapping.confluence_page_id > 0:
        page_id = mapping.confluence_page_id
    else:
        error_text = 'Sorry! Your retrospective note could not be saved at this time (error: confluence page to record notes does not exist or is not mapped here).  Please contact your slack administrator or the producer for this project and request they configure this channel for recording confluence notes.'
        app.logger.error(error_text)
        slack_message = format_slack_response_message('Error', message_text=error_text, color='danger')
        return jsonify(slack_message)

    # if we made it this far we have a valid retro note and a place to save it in confluence
    conf_api = ConfluenceAPI()
    # expect this to return object with either an error message or note that it worked
    update_result = conf_api.update_retrospective_page_content(page_id, note_text, user_name)

    app.logger.info('update_result: {}'.format(update_result))

    if 'error' in update_result:
        error_text = 'Sorry! Your retrospective note could not be saved at this time (error: confluence page could not be found and/or updated via API).  Please manually add your note to the correct page in confluence.'
        app.logger.error(error_text)
        slack_message = format_slack_response_message('Error', message_text=error_text, color='danger')
        return jsonify(slack_message)
    elif 'success' in update_result:
        # should return url of page we updated
        response_text = 'Thank you! Your retrospective note has been saved in confluence: {}'.format(update_result['success'])
        app.logger.info(response_text)
        slack_message = format_slack_response_message('Retrospective note saved!', message_text=response_text, color='good', title_link=update_result['success'])
        return jsonify(slack_message)
