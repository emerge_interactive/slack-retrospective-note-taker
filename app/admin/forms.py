from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField, HiddenField
from wtforms.validators import DataRequired


class AdminLoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class MappingForm(FlaskForm):
    id = HiddenField()
    channel = StringField('Slack channel name', validators=[DataRequired()])
    confluence_page_id = IntegerField('Confluence page ID', validators=[DataRequired(message="This field is required and must be a number.")])
