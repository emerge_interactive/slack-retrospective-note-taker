from flask import Blueprint, render_template, request, session, url_for, abort, redirect
from flask import current_app as app
import os

from .forms import AdminLoginForm, MappingForm
from ..models.models import db, Mapping


admin = Blueprint(
    'admin', __name__,
    template_folder='templates'
)

def login_required(f):

    def wrapper(*args, **kwargs):
        if 'username' not in session:
            abort(401)

        return f(*args, **kwargs)

    # Renaming the function name to avoid error (https://stackoverflow.com/a/42254713):
    wrapper.__name__ = f.__name__
    return wrapper


@admin.route('/admin/logout', methods=['GET'])
def logout():
    if 'username' in session:
        app.logger.info('logout attempt for username: {}'.format(session['username']))
        session.clear()

    return redirect(url_for('admin.login'))


@admin.route('/admin/login', methods=['GET', 'POST'])
def login():
    ADMIN_USERNAME = os.getenv('ADMIN_USERNAME', 'admin')
    ADMIN_PASSWORD = os.getenv('ADMIN_PASSWORD', 'iloveagile')

    # already logged in?
    if request.method == 'POST' and 'username' in session:
        return redirect(url_for('admin.mappings_list'))

    form = AdminLoginForm()
    if request.method == 'POST' and form.validate():
        form = AdminLoginForm(request.form)
        app.logger.info('login attempt for username: {}'.format(form.username.data))
        # validate simple login
        if form.username.data == ADMIN_USERNAME and form.password.data == ADMIN_PASSWORD:
            app.logger.info('valid login!')
            # set username to session (this is our auth)
            session['username'] = form.username.data
            return redirect(url_for('admin.mappings_list'))

    return render_template('admin_login.html', form=form)


@admin.route('/admin/dashboard', methods=['GET'])
@login_required
def mappings_list():
    """Admin interface dashboard
    """
    mappings = Mapping.query.all()
    app.logger.debug('mappings: {}'.format(mappings))

    return render_template('mappings.html', mappings=mappings)


@admin.route('/admin/add_slack_confluence_mapping', methods=['GET', 'POST'])
@login_required
def add_slack_confluence_mapping():
    """Admin to add a slack channel to confluence page id mapping
    """
    mapping = None
    form = MappingForm()
    if request.method == 'POST' and form.validate():
        form = MappingForm(request.form)
        app.logger.debug('edit mapping form POSTed and validated. form: {}'.format(form))

        # make sure we do not already have a mapping for this channel
        if db.session.query(Mapping.id).filter_by(channel=form.channel.data).scalar() is None:
            mapping = Mapping(
                channel=form.channel.data,
                confluence_page_id=form.confluence_page_id.data
            )
            db.session.add(mapping)
            db.session.commit()
            return redirect(url_for('admin.mappings_list'))
        else:
            app.logger.warn('mapping already exists for channel name: {}'.format(form.channel.data))
            tmp_errors = list(form.channel.errors)
            tmp_errors.append('A mapping already exists for this channel name. Please remove or update that one.')
            form.channel.errors = tuple(tmp_errors)

    return render_template('mapping.html', form=form, mapping=mapping)


@admin.route('/admin/edit_slack_confluence_mapping/<mapping_id>', methods=['GET', 'POST'])
@login_required
def edit_slack_confluence_mapping(mapping_id):
    """Admin to edit a slack channel to confluence page id mapping
    """
    mapping = Mapping.query.get(mapping_id)
    app.logger.debug('id: {}'.format(mapping_id))
    app.logger.debug('found specific mapping: {}'.format(mapping))

    form = MappingForm()
    form.id.data = str(mapping.id)
    form.channel.data = mapping.channel
    form.confluence_page_id.data = mapping.confluence_page_id

    if request.method == 'POST' and form.validate():
        form = MappingForm(request.form)
        app.logger.debug('edit mapping form POSTed and validated. form: {}'.format(form))
        mapping.channel = form.channel.data
        mapping.confluence_page_id = form.confluence_page_id.data
        db.session.commit()
        
        return redirect(url_for('admin.mappings_list'))

    return render_template('mapping.html', form=form, mapping=mapping)


@admin.route('/admin/delete_slack_confluence_mapping/<mapping_id>', methods=['GET', 'POST'])
@login_required
def delete_slack_confluence_mapping(mapping_id):
    """Admin to delete a slack channel to confluence page id mapping
    """
    app.logger.info('deleting mapping for id: {}'.format(mapping_id))
    mapping = Mapping.query.get(mapping_id)
    db.session.delete(mapping)
    db.session.commit()
    return redirect(url_for('admin.mappings_list'))
