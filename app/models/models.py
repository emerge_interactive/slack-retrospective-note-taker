from .. import db
from datetime import datetime


class Mapping(db.Model):
    __tablename__ = 'confluence_slack_mapping'

    id = db.Column(db.Integer, primary_key=True)
    channel = db.Column(db.String())
    confluence_page_id = db.Column(db.Integer)
    created_date = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Mapping {self.channel} - {self.confluence_page_id}>'
