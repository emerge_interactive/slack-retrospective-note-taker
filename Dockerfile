FROM python:3.7-alpine

EXPOSE 5000
WORKDIR /code
COPY requirements.txt requirements.txt

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev && \
  python3 -m pip install -r requirements.txt --no-cache-dir
