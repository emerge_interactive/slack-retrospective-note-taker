# Slack retrospective note recorder
A basic python/flask API to save retrospective notes in confluence from Slack, using a [slash command][slash].

The basic flow of how this works is:

1. In Slack, a user uses the `/retro` slash command
1. Slack POSTS slash message to this API
1. this API parses & validates POST from Slack (*this includes [validating the signature][slackverify] from the slack request*)
1. this API updates (PUT) a defined page in a Confluence space (via the Atlassian API)
1. this API responds to Slack and a message is displayed to user (in Slack)

The API application also includes a password protected web interface for managing the Slack channel to Confluence page mappings.

---

## Set up

To use this API you will need:

* a place to host this python/flask/postgres API (where it can respond to https requests)
* a Confluence instance (and page(s)) to record the retrospective notes
* a Slack instance

### 1. Confluence configuration/setup

1. Because this API uses the Atlassian API to write to Confluence, you'll need to generate an Atlassian API token for it to use.  Follow [these directions][atlapitoken] and note your username/token for later use
1. Create Confluence page(s) for recording retrospective notes (*we do this per project*)
    1. this python API is expecting the confluence page where it writes the retrospective note to look a very specific way (have specific HTML).  **The body of the Confluence page must contain the following html table, and it must be the only html table**:
    ```
    <table class="wrapped confluenceTable" resolved=""><colgroup><col><col><col></colgroup><tbody><tr><th class="confluenceTh">Retrospective note</th><th class="confluenceTh">Created</th><th class="confluenceTh">Created by (slack username)</th></tr></tbody></table>
    ```
    1. *To reduce errors in setting this page up and make it easy for your users to create, it is recommended you create a global [Confluence page template](https://confluence.atlassian.com/conf64/create-a-template-936510776.html) for recording retrospective notes.*

### 2. Deploy this API

This API will need to be publicly available so Slack can POST to it (using HTTPS).

Hosting requirements/instructions:

1. must use python 3.7+
1. python depedencies must be installed (can be found in `requirements.txt` file)
1. postgres - used to store slack channel to confluence page id mappings
1. the following environment variables must exist in the hosting environment:
    * `FLASK_SECRET_KEY` - string - required for encryption
    * `FLASK_APP` - string - path to flask app
    * `CONFLUENCE_BASE_URL` - URL string - the base URL of your Confluence API (example: `https://yourcompany.atlassian.net/wiki/rest/api/`)
    * `ATLASSIAN_AUTH_USER` - email string - part of the [Atlassian API token][atlapitoken] you generated
    * `ATLASSIAN_AUTH_PASSWORD` - token string - part of the [Atlassian API token][atlapitoken] you generated
    * `SLACK_SIGNING_SECRET` - string - this value can be found from the Slack app you create, in the "credentials" section
    * `SLACK_VNUM` - string - currently should be set to `v0` - more info in Slack's documentation about [signed secrets][slackverify]
    * `DATABASE_URL` - URI - valid postgres database connection URI
    * `ADMIN_USERNAME` - string - the username for signing into the admin panel (*default is `admin`*)
    * `ADMIN_PASSWORD` - string - the password for signing into the admin panel (*default is `iloveagile`*)


### 3. Create Slack app and slash command

Finally, within your Slack instance, you need to create an App and register a slash command.  Follow [these directions](https://api.slack.com/interactivity/slash-commands) to create.  Info you will need:

* command name: we use `/retro`, but you can use whatever you want!
* request URL: url of the API you want slack to send POST to

Note the signing secret for the Slack app you create (in "credentails section").  You will set the `SLACK_SIGNING_SECRET` envrionment variable in the python API to this value.

---

## Using the mappings admin

The API application also includes a web based administration interface for managing the Slack to Confluence page mappings.  **The mappings admin is accessable at `/admin/login`**.  The username and password are defined by the `ADMIN_USERNAME` and `ADMIN_PASSWORD` environment variables.

### Backing up mappings

The mappings (Slack channel name to Confluence page ID) are stored in a postgres database.  If you want to back up those mappings, you'll need to back up the contents of whatever postgres database you are writing to (*found in `DATABASE_URL` environment variable*).

---

## Local development instructions

1. make sure you have an [Atlassian API token][atlapitoken]
1. clone repository and `cd` into directory
1. create local web environment variables file and populate:
    1. create file from template: `cp docker_config/local.web.env.template docker_config/local.web.env`
    1. edit the contents of the `local.web.env` file and set the following:
        * `ATLASSIAN_AUTH_USER` and `ATLASSIAN_AUTH_PASSWORD` to your Atlassian API token values
        * `CONFLUENCE_BASE_URL` to the base URL of your Confluence API (example: `https://yourcompany.atlassian.net/wiki/rest/api/`)
        * `DATABASE_URL` to a valid postgres connection URI with appropriate db credentials (example: `postgres://db_user:password@db:5432/slack_retro_notes`)
        * modify any other environment variables as you see fit
1. create local database environment variables file and populate:
    1. create file from template: `cp docker_config/local.db.env.template docker_config/local.db.env`
    1. edit the contents of the `local.db.env` and set the [standard postgres datbase credentials](https://hub.docker.com/_/postgres) (**these should match those used in `DATABASE_URL` env var for your web container**)
1. start the docker container: `docker-compose up`
1. the API should be available at: http://localhost:5000 and the admin at http://localhost:5000/admin/login

You can test the API locally using [Postman](https://www.getpostman.com/) or similar.  Make a POST to the `/retro_note` endpoint with the following attributes:

* headers:
    * `Content-Type: application/x-www-form-urlencoded`
* body params - see [Slack documentation](https://api.slack.com/slash-commands#app_command_handling) for what a slash command sends in POST body

*Note that the Slack request verification can be skipped when run locally, by setting the `SKIP_SLACK_SIGNING=True` environment variable.*


[slash]: https://api.slack.com/slash-commands "Slack slash command"
[atlapitoken]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html "Atlassian API token"
[slackverify]: https://api.slack.com/docs/verifying-requests-from-slack "Verifying slack request"
