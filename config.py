import os
import logging

class Config:
    """Set Flask configuration vars"""

    FLASK_DEBUG = os.getenv('FLASK_DEBUG', 0)
    FLASK_ENV = os.getenv('FLASK_ENV', 'production')
    SECRET_KEY = os.environ['FLASK_SECRET_KEY']

    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_ECHO = os.getenv('SQLALCHEMY_ECHO', False)
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS', False)

    LOGGING_LEVEL = logging.INFO
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

    SLACK_SIGNING_SECRET = os.environ['SLACK_SIGNING_SECRET']
    SLACK_VNUM = os.environ['SLACK_VNUM']
    # env var to skip slack request validation in local dev (default this to NOT SKIP)
    SKIP_SLACK_SIGNING = os.getenv('SKIP_SLACK_SIGNING', False)

    CONFLUENCE_BASE_URL = os.environ['CONFLUENCE_BASE_URL']
    ATLASSIAN_AUTH_USER = os.environ['ATLASSIAN_AUTH_USER']
    ATLASSIAN_AUTH_PASSWORD = os.environ['ATLASSIAN_AUTH_PASSWORD']
